from setuptools import setup, find_packages

description = '''\
SQLLine is a Python command-line utility for connecting to databases and
executing SQL commands, inspired by the original
Java-based SQLLine (http://sqlline.sf.net). It uses the standard
Python DB API, so it can be used with any database module.
'''

setup(
    name='sqlline',
    version='0.1',
    author='Lukas Lalinsky',
    author_email='lukas@oxygene.sk',
    description=description,
    url='https://bitbucket.org/lalinsky/python-sqlline',
    license='BSD',
    packages=find_packages(),
    install_requires=[
        'sqlparse',
        'tabulate',
    ],
    entry_points={
        'console_scripts': [
            'pysqlline = sqlline.shell:main',
        ],
    },
)
