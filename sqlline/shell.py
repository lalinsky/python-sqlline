# Copyright (c) 2015, Lukas Lalinsky <lukas@oxygene.sk>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Inspired by http://sqlline.sourceforge.net/

import argparse
import time
import importlib
import os
import platform
import readline
import sqlparse
import tabulate
import traceback


class SqlLine(object):

    if platform.system() == 'Windows':
        history_file = os.path.join("~", "pysqlline", "history")
    else:
        history_file = os.path.join("~", ".local", "share", "pysqlline", "history")

    def __init__(self):
        self.module = None
        self.connection = None
        self.load_history()

    def load_history(self):
        try:
            history_file = os.path.expanduser(self.history_file)
            readline.read_history_file(history_file)
        except IOError:
            pass

    def save_history(self):
        try:
            history_file = os.path.expanduser(self.history_file)
            history_dir = os.path.dirname(history_file)
            if not os.path.isdir(history_dir):
                os.makedirs(history_dir)
            readline.write_history_file(history_file)
        except IOError:
            pass

    def close(self):
        self.save_history()
        self.disconnect()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def connect(self, module, params):
        self.module = importlib.import_module(module)
        self.connection = self.module.connect(params)

    def reconnect(self, params):
        self.disconnect()
        self.connection = self.module.connect(params)

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    def execute_command(self, command):
        command = command.split()
        if command[0] == "!commit":
            self.connection.commit()
        elif command[0] == "!rollback":
            self.connection.rollback()
        elif command[0] == "!quit":
            self.running = False
        elif command[0] == "!help":
            print "!autocommit         Set autocommit mode on or off"
            print "!commit             Commit the current transrow_action (if autocommit is off)"
            print "!quit               Exits the program"
            print "!rollback           Roll back the current transrow_action (if autocommit is off)"
        elif command[0] == "!autocommit":
            if len(command) > 1:
                if command[1] == "on":
                    self.connection.autocommit = True
                elif command[1] == "off":
                    self.connection.autocommit = False
            print "Autocommit status: {}".format("on" if self.connection.autocommit else "off")
        else:
            raise self.module.NotSupporterError('unknown command', command)

    def execute_statement(self, statement):
        with self.connection.cursor() as cursor:
            t0 = time.time()
            cursor.execute(statement)
            row_action = "affected"
            row_count = cursor.rowcount
            if cursor.description is not None:
                headers = []
                for column in cursor.description:
                    headers.append(column[0])
                rows = cursor.fetchall()
                print tabulate.tabulate(rows, headers=headers, tablefmt="grid")
                row_action = "selected"
                if row_count == -1:
                    row_count = len(rows)
            messages = []
            if row_count == 0:
                messages.append("no rows {}".format(row_action))
            elif row_count == 1:
                messages.append("1 row {}".format(row_action))
            elif row_count > 1:
                messages.append("{} rows {}".format(row_count, row_action))
            messages.append("({:.3f} seconds)".format(time.time() - t0))
            print " ".join(messages)

    def catch_db_errors(self, func, *args, **kwargs):
        try:
            func(*args, **kwargs)
        except self.module.Error as error:
            traceback.print_exc()

    def run(self):
        buffer = ""
        self.running = True
        while self.running:
            try:
                if not buffer:
                    buffer = raw_input('db=> ')
                else:
                    buffer += raw_input('db-> ')
            except (KeyboardInterrupt, EOFError):
                print
                break
            if buffer.startswith("!"):
                self.catch_db_errors(self.execute_command, buffer.strip())
                buffer = ""
            else:
                statements = sqlparse.split(buffer)
                while statements:
                    if statements[0].endswith(';'):
                        statement = statements.pop(0).rstrip(';').strip()
                        self.catch_db_errors(self.execute_statement, statement)
                    else:
                        break
                if statements:
                    buffer = "\n".join(statements) + "\n"
                else:
                    buffer = ""


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--module', '-m')
    parser.add_argument('params')
    args = parser.parse_args()
    with SqlLine() as sqlline:
        sqlline.connect(args.module, args.params)
        sqlline.run()
