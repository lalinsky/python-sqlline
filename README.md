# Python-SQLLine

Python-SQLLine is a Python command-line utility for connecting to databases and executing SQL commands. It uses the standard Python DB API, so it can be used with any database module.                                                              

For example, connecting to a PostgreSQL database:

    pysqlline -m psycopg2 'user=myusername dbname=mydb'

The project was inspired by the original Java-based [SQLLine](http://sqlline.sf.net) created by Marc Prud'hommeaux. This version is nowhere near as complete, but it can be useful for experimenting with databases that do not have a native command-line shell, but do have a Python module.